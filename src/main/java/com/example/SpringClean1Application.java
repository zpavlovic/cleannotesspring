package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringClean1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringClean1Application.class, args);
	}
}
