package com.example.notes.data.spring;

import java.util.List;

import com.example.notes.domain.notes.Note;

public interface NotesDataSource {
	
	Note addNote(Note note);
	
	void deleteNote(Note note);
	
	Note editNote(Note note);
	
	List<Note> findAll();
}
