package com.example.notes.data.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.notes.data.spring.entity.NoteJpa;
import com.example.notes.data.spring.entity.mapper.NoteJpaMapper;
import com.example.notes.data.spring.service.NoteService;
import com.example.notes.domain.notes.Note;

public class NotesDataSourceJpa implements NotesDataSource {
	
	private NoteService notesService;
	
	public NotesDataSourceJpa(NoteService notesService) {
		// TODO Auto-generated constructor stub
		this.notesService = notesService;
	}

	@Override
	public Note addNote(Note note) {
		NoteJpa savedNotesJpa = notesService.addNote(NoteJpaMapper.getInstance().convertToJpaModel(note));
		return NoteJpaMapper.getInstance().convertToPlainModel(savedNotesJpa);
	}

	@Override
	public void deleteNote(Note note) {
		notesService.deleteNote(NoteJpaMapper.getInstance().convertToJpaModel(note));		
	}

	@Override
	public Note editNote(Note note) {
		NoteJpa editNoteJpa = notesService.editNote(NoteJpaMapper.getInstance().convertToJpaModel(note));
		return NoteJpaMapper.getInstance().convertToPlainModel(editNoteJpa);
	}

	@Override
	public List<Note> findAll() {
		List<NoteJpa> notesJpa = notesService.findAll();
		List<Note> notes = new ArrayList<>();
		for(NoteJpa noteJpa : notesJpa){
			notes.add(NoteJpaMapper.getInstance().convertToPlainModel(noteJpa));
		}
		return notes;
	}

}
