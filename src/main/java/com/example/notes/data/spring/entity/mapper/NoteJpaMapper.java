package com.example.notes.data.spring.entity.mapper;

import com.example.notes.data.spring.entity.NoteJpa;
import com.example.notes.domain.notes.Note;

public class NoteJpaMapper {
	
	public static NoteJpaMapper instance;
	
	public static NoteJpaMapper getInstance(){
		if(instance == null){
			return instance = new NoteJpaMapper();
		}
		return instance;
	}
	
	public Note convertToPlainModel(NoteJpa noteJpa){
		return new Note(noteJpa.getId(), noteJpa.getText(), noteJpa.getDate());
	}
	
	public NoteJpa convertToJpaModel(Note note){
		return new NoteJpa(note.getId(), note.getText(), note.getDate());
	}

}
