package com.example.notes.data.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.notes.data.spring.entity.NoteJpa;

public interface NotesJpaRepository  extends JpaRepository<NoteJpa, Integer>{
	
	@Query("SELECT n FROM NoteJpa n ORDER BY n.id DESC")
	List<NoteJpa> findAll();
}
