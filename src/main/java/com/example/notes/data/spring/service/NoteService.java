package com.example.notes.data.spring.service;

import java.util.List;

import com.example.notes.data.spring.entity.NoteJpa;



public interface NoteService {
	
	NoteJpa addNote(NoteJpa note);
	
	void deleteNote(NoteJpa note);
	
	NoteJpa editNote(NoteJpa note);
	
	List<NoteJpa> findAll();
}
