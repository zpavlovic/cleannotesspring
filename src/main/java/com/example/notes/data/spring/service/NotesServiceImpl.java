package com.example.notes.data.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.notes.data.spring.entity.NoteJpa;
import com.example.notes.data.spring.repository.NotesJpaRepository;

@Service
@Transactional
public class NotesServiceImpl implements NoteService{
	
	@Autowired
	private NotesJpaRepository notesRepository;

	@Override
	public NoteJpa addNote(NoteJpa note) {
		return notesRepository.save(note);
	}

	@Override
	public void deleteNote(NoteJpa note) {
		notesRepository.delete(note);		
	}

	@Override
	public NoteJpa editNote(NoteJpa note) {
		return notesRepository.save(note);
	}

	@Override
	public List<NoteJpa> findAll() {
		return notesRepository.findAll();
	}

}
