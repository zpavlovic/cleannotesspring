package com.example.notes.domain.notes;

public interface AddNotesInteractor {
	Note addNote(Note note);
}
