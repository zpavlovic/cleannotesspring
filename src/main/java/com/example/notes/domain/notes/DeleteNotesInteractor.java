package com.example.notes.domain.notes;

public interface DeleteNotesInteractor {
	public void deleteNote(Note note);
}
