package com.example.notes.domain.notes;

public interface EditNotesInteractor {
	Note editNote(Note note);
}
