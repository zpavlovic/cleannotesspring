package com.example.notes.domain.notes;

public class NotesInteractorImpl implements AddNotesInteractor, DeleteNotesInteractor, EditNotesInteractor{
	
	private NotesRepository notesRepository;
	
	public NotesInteractorImpl(NotesRepository notesRepository){
		this.notesRepository = notesRepository;
	}

	@Override
	public void deleteNote(Note note) {
		notesRepository.deleteNote(note);		
	}

	@Override
	public Note addNote(Note note) {
		return notesRepository.addNote(note);		
	}

	@Override
	public Note editNote(Note note) {
		return notesRepository.editNote(note);		
	}
}
