package com.example.notes.domain.notes;

import java.util.List;

public interface NotesRepository {
	
	Note addNote(Note note);
	
	void deleteNote(Note note);
	
	Note editNote(Note note);
	
	List<Note> findAll();

}
