package com.example.notes.domain.notes;

import java.util.List;

import com.example.notes.data.spring.NotesDataSourceJpa;

public class NotesRepositoryImpl implements NotesRepository {
	
	private NotesDataSourceJpa notesDataSourceJpa;
	
	public NotesRepositoryImpl(NotesDataSourceJpa notesDataSourceJpa) {
		this.notesDataSourceJpa = notesDataSourceJpa;
	}
	

	@Override
	public Note addNote(Note note) {
		return notesDataSourceJpa.addNote(note);
	}

	@Override
	public void deleteNote(Note note) {
		 notesDataSourceJpa.deleteNote(note);		
	}

	@Override
	public Note editNote(Note note) {
		return notesDataSourceJpa.editNote(note);
	}

	@Override
	public List<Note> findAll() {
		return notesDataSourceJpa.findAll();
	}

}
