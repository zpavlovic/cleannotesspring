package com.example.notes.domain.notes;

import java.util.List;

public interface NotesView {

	public List<Note> showAllNotes();
	
	public void showAddNote();
	
	public void showEditNote(Note note);
	
}
