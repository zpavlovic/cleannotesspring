package com.example.notes.presentation.presenter;

import com.example.notes.domain.notes.Note;

public interface NotesPresenter {
	
	void addNote(Note note);
	
	void deleteNote(Note note);
	
	void editNote(Note note);
	
	void showAddNote();
	
	void showEditNote(Note note);
	
	void showAllNotes();

}
