package com.example.notes.presentation.presenter;

import com.example.notes.domain.notes.Note;
import com.example.notes.domain.notes.NotesInteractorImpl;
import com.example.notes.domain.notes.NotesView;

public class NotesPresenterImpl  implements NotesPresenter{
	
	private NotesInteractorImpl notesInteractorImpl;
	private NotesView notesView;
	
	public NotesPresenterImpl(NotesView notesView, NotesInteractorImpl notesInteractorImpl) {
		this.notesView = notesView;
		this.notesInteractorImpl = notesInteractorImpl;
	}

	@Override
	public void addNote(Note note) {
		notesInteractorImpl.addNote(note);		
	}

	@Override
	public void deleteNote(Note note) {
		notesInteractorImpl.deleteNote(note);		
	}

	@Override
	public void editNote(Note note) {
		notesInteractorImpl.editNote(note);		
	}

	@Override
	public void showAddNote() {
		notesView.showAddNote();		
	}

	@Override
	public void showEditNote(Note note) {
		notesView.showEditNote(note);		
	}

	@Override
	public void showAllNotes() {
		notesView.showAllNotes();		
	}

}
