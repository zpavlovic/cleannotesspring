package com.example.notes.presentation.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.notes.data.spring.NotesDataSourceJpa;
import com.example.notes.data.spring.service.NoteService;
import com.example.notes.domain.notes.Note;
import com.example.notes.domain.notes.NotesRepository;
import com.example.notes.domain.notes.NotesRepositoryImpl;
import com.example.notes.domain.notes.NotesView;

@Controller
public class NotesController implements NotesView {
	
	private List<Note> notes;
	
	private NotesRepository notesRepository;
	private NotesDataSourceJpa notesDataSourceJpa;
	
	@Autowired
	private NoteService notesService;
	
	@RequestMapping(value = "/")
	public String index(Model model){	
		model.addAttribute("notes", showAllNotes());
		return "index";
	}

	@Override
	public List<Note> showAllNotes() {
		notesDataSourceJpa = new NotesDataSourceJpa(notesService);
		notesRepository = new NotesRepositoryImpl(notesDataSourceJpa);
		notes = notesRepository.findAll();	
		return notes;
	}

	@Override
	public void showAddNote() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showEditNote(Note note) {
		// TODO Auto-generated method stub
		
	}

}
